# Parrot [![Build Status](https://travis-ci.org/zooo/parrot.svg?branch=master)](https://travis-ci.org/zooo/parrot.svg)

Parrot is a sandbox.

- GitHub location [https://github.com/zooo/parrot](https://github.com/zooo/parrot)
- BitBucket location [https://bitbucket.org/oxo/parrot](https://bitbucket.org/oxo/parrot)

```
$ git remote rename origin github
$ git remote add bitbucket https://oxo@bitbucket.org/oxo/parrot.git
$ git push github master
$ git push bitbucket master
```

Try add a branch. This time is real.

Set up Slack. Last time is for GitHub, this time for BitBucket again.
